extends RigidBody


const CLICK_FORCE: Vector3 = Vector3(0.0, -30.0, 0.0)
const MAX_VELOCITY: float = 100.0


var sounds = [
	preload("res://audio/olive_pop1.ogg"),
	preload("res://audio/olive_pop2.ogg"),
	preload("res://audio/olive_pop3.ogg"),
	preload("res://audio/olive_pop4.ogg"),
	preload("res://audio/olive_pop5.ogg"),
	preload("res://audio/olive_pop6.ogg"),
	preload("res://audio/olive_pop7.ogg"),
	preload("res://audio/olive_pop8.ogg")
]
var olive_pop1: Resource = preload("res://audio/olive_pop1.ogg")
var olive_pop2: Resource = preload("res://audio/olive_pop2.ogg")
var olive_pop3: Resource = preload("res://audio/olive_pop3.ogg")
var olive_pop4: Resource = preload("res://audio/olive_pop4.ogg")
var olive_pop5: Resource = preload("res://audio/olive_pop5.ogg")
var olive_pop6: Resource = preload("res://audio/olive_pop6.ogg")
var olive_pop7: Resource = preload("res://audio/olive_pop7.ogg")
var olive_pop8: Resource = preload("res://audio/olive_pop8.ogg")


onready var sound = get_node("AudioStreamPlayer3D")


func _physics_process(delta):
	if get_linear_velocity().length() > MAX_VELOCITY:
		set_linear_velocity(get_linear_velocity().normalized() * MAX_VELOCITY)

func _on_Olive_input_event(camera, event, position, normal, shape_idx):
	if event is InputEventMouseButton and event.is_pressed():
		print(event)
		print(position)
		apply_impulse(position, CLICK_FORCE)

		sound.stream = sounds[rand_range(0, sounds.size() - 1)]
		print(sound.stream.resource_path)
		sound.pitch_scale = 1.0 + ((randf() - 0.5) * 0.1)
		sound.play()
